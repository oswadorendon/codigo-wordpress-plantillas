<?php
/*
Template Name: maestros
*/
get_header(); 
?>
<div id="page" class="content">
	<div id="content" class="widecolumn">
	<div>
		Reporte de Maestros
	</div>
	<br>
	<fieldset>
				<form>
					<table>
				<tr>
					<td>
						<label>Semestre</label>
					</td>
					<td>
				  		<select id='cbSem' name='cbSem' style="width:auto;">
				  			<option value='-1'>Seleccione...</option>					 		
						</select>
					</td>
					<td>
						<label>Curso</label>
					</td>
					<td>
				  		<select id='cbCursos' name='cbCursos' style="width:auto;">
				  			<option value='-1'>Seleccione...</option>					 		
						</select>
					</td>						
				</tr>
				<tr>
					<td>
						<label>Grupo</label>
					</td>
					<td>							
						<select id="cbGpos" name="cbGpos" >
							<option value='-1'>Seleccione...</option>
						</select>
					</td>
					<td>
						<label>Clave</label>
					</td>
					<td>
						<input type="text" id='txtClave' name='txtClave' >
					</td>					
				</tr>
				<tr>
					<td>
						<label>Tiempo Curso</label>
					</td>
					<td>
						<input id="txtTiempocurso" name="txtTiempocurso"  type="text" >
					</td>																				
					<td>
						<input type='submit' value='Buscar...' > 
					</td>
				</tr>	
			</table>
		</form>		
		</fieldset>	
		<div id='dtCursos'>
					<table class='resultado' border="0" cellspacing="0" cellpadding="0">
					 <p align="left">					    										
								<form method="post">
			                       <p>Listado de Maestros 
			                         <input type="submit" value="Exportar" /></p>
			                    </form>						
					  </p>
					  <tr>
					<th class="tLabel" scope="col" style="width:120px;">Clave</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Maestro</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Curso</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Grupo</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Alumnos</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">F. Inicio</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">F. Final</th>				    
					  </tr>
					  <tr>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>					  	
					  </tr>
					</table>
			</div>
	</div>
<div>
<?php get_footer(); ?>