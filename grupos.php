<?php
/*
Template Name: grupos
*/
get_header(); 
?>
<div id="page" class="content">
	<div id="content" class="widecolumn">
		<div>
			<h2>Reporte de Grupos</h2>		
		</div>	
		<br>
			<fieldset>
					<form>
						<table>
					<tr>
						<td>
							<label>Semestre</label>
						</td>
						<td>
					  		<select id='cbSem' name='cbSem' style="width:auto;">
					  			<option value='-1'>Seleccione...</option>					 		
							</select>
						</td>
						<td>
							<label>Curso</label>
						</td>
						<td>
					  		<select id='cbCursos' name='cbCursos' style="width:auto;">
					  			<option value='-1'>Seleccione...</option>					 		
							</select>
						</td>						
					</tr>
					<tr>
						<td>
							<label>Grupo</label>
						</td>
						<td>							
							<select id="cbGpos" name="cbGpos" >
								<option value='-1'>Seleccione...</option>
							</select>
						</td>
						<td>
							<label>Tutor</label>
						</td>
						<td>
							<input type="text" id='txttutor' name='txttutor' >
						</td>					
					</tr>
					<tr>
						<td>
							<label>Fecha</label>
						</td>
						<td>
							<input id="eventdate" name="Selectcurso"  type="text" >
						</td>
						<td>
							<label>Alumnos</label>
						</td>
						<td>
							<input id="txtnumalum" name="txtnumalum"  type="text" >
						</td>														
						<td>
							<input type='submit' value='Buscar...' > 
						</td>
					</tr>	
				</table>
			</form>		
		</fieldset>	
		<div id='dtCursos'>
					<table class='resultado' border="0" cellspacing="0" cellpadding="0">
					 <p align="left">					    										
								<form method="post">
			                       <p>Listado de Grupos 
			                         <input type="submit" value="Exportar" /></p>
			                    </form>						
					  </p>
					  <tr>
					<th class="tLabel" scope="col" style="width:120px;">Semestre</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Curso</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Grupo</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Tutor</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Alumnos</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">%avance</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">%atraso</th>				    
					  </tr>
					  <tr>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>					  	
					  </tr>
					</table>
		</div>
	</div>
<div>
<?php get_footer(); ?>