<?php
/*
Template Name: actividad
*/
get_header(); 
?>
<div id="page" class="content">
	<div id="content" class="widecolumn">
		<div id="content_filtros"  >
			<h2>Reporte de actividad</h2>
			<br>
			<fieldset>
			<form>
				<table>
				<tr>
					<td>
						<label>Semestre</label>
					</td>
					<td>
						<select id='cbSemestres' >
							<option value='-1'>Seleccione..</option>
						</select>
					</td>
					<td>
						<label>Curso</label>
					</td>
					<td>
				  		<select id='cbCursos' name='cbCursos' style="width:auto;">
				  			<option value='-1'>Seleccione...</option>					 		
						</select>
					</td>
				</tr>
				<tr>	
					<td>
						<label>Grupo</label>
					</td>
					<td>							
						<select id="cbGpos" name="cbGpos" >
							<option value='-1'>Seleccione...</option>
						</select>
					</td>
					<td>
						<label>Matricula</label>
					</td>
					<td>
						<input type="text" id='txtmatricula' name='txtmatricu' >
					</td>
				</tr>
				<tr>					
					<td>
						<label>Fecha</label>
					</td>
					<td>
						<input id="eventdate" name="Selectcurso"  type="text" >
					</td>
					<td>
						<label>Semestre</label>
					</td>
					<td>
						<input type="text" name="txtsem" id="txtsemestre" >
					</td>					
					<td>
						<input type='submit' value='Buscar...' > 
					</td>
				</tr>					
			</table>
		</form>		
		</fieldset>	
		<div id='dtCursos'>
					<table class='resultado' border="0" cellspacing="0" cellpadding="0">
					 <p align="left">					    										
								<form method="post">
			                       <p>Listado de Actividades 
			                         <input type="submit" value="Exportar" /></p>
			                    </form>						
					  </p>
					  <tr>
					<th class="tLabel" scope="col" style="width:120px;">Fecha</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Materia</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Grupo</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Matricula</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Alumno</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Actividad</th>
				    <th class="th" scope="col" style="width:80px;">&nbsp;</th>
				    <th class="tLabel" scope="col" style="width:120px;">Tiempo</th>				    
					  </tr>
					  <tr>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>
					  	<td class="trowspace"></td>
					  	<td class="trowlabel">&nbsp;</td>					  	
					  </tr>
					</table>
				</form>
			</div>
		</div>	
	</div>
<div>
<?php get_footer(); ?>